// teste lógico ? se verdadeiro : se falso

let n1 = 10
let n2 = 20
let n3 = 30
let n4 = 40

// se n1 == n2 faça....
n1 == n2 ? console.log("são iguais") : console.log('São diferentes')


// colocando em uma variável
const n5 = n3 >= n4 ? `${n3} é maior que ${n4}` : `${n3} não é maior que ${n4}` 
console.log(n5)

// colocando tudo em um console.log
console.log(n5 ? 'verdadeiro': 'falso')
